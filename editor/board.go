package editor

import (
	"image"
	"image/color"

	"gioui.org/f32"
	"gioui.org/io/pointer"
	"gioui.org/layout"

	"gioui.org/op"
	"gioui.org/op/clip"
	"gioui.org/op/paint"

	"gotimap/asset"

	"fmt"
)

type MapBlock struct {
	X, Y, H, W int
	conflict   bool
}

type Board struct {
	W, H   int
	Layers map[int]map[string][]*MapBlock
}

func (b *Board) AddTile(t Tile, l, x, y int) {
	if b.Layers[l] == nil {
		b.Layers[l] = make(map[string][]*MapBlock)
	}
	b.Layers[l][t.ImgFile] = append(b.Layers[l][t.ImgFile], &MapBlock{X: x, Y: y, H: t.H, W: t.W})
}

var (
	Grey color.RGBA = color.RGBA{R: 0x33, G: 0x33, B: 0x33, A: 0x33}
)

type BoardEditor struct {
	Board *Board

	tileSize int
	scale    float32
	minScale f32.Point

	xOff, yOff float32

	currentLayer int
	highLight    image.Point
	blockLight   []image.Rectangle

	selImg    Tile
	selImgRdr image.Image

	close chan (int)
}

func NewBoardEditor(h, w int, selector *TileList) *BoardEditor {
	b := &BoardEditor{}

	b.tileSize = 140
	b.Board = &Board{H: h, W: w}
	b.Board.Layers = make(map[int]map[string][]*MapBlock)

	b.highLight = image.Point{X: -1, Y: -1}

	b.close = make(chan int)

	go func() {
		for {
			select {
			case t := <-selector.SelectedTile:
				b.selImg = t
				b.selImgRdr = nil
			case <-b.close:
				return
			}
		}
	}()

	return b
}

func (t *BoardEditor) Widget() layout.Widget {
	return func(gtx layout.Context) layout.Dimensions { return t.Layout(gtx) }
}

func (b *BoardEditor) Layout(gtx layout.Context) layout.Dimensions {
	//Avoid affecting the input tree with pointer events.
	op.Push(gtx.Ops).Pop()

	scaleMin := float32(gtx.Constraints.Max.X) / float32(b.Board.W*b.tileSize)
	scaleOther := float32(gtx.Constraints.Max.Y) / float32(b.Board.H*b.tileSize)

	if scaleMin > scaleOther {
		scaleMin = scaleOther
	}

	//Probably a resize
	if b.scale < scaleMin {
		b.applyMinScale(gtx, scaleMin)
	}

	for _, e := range gtx.Events(b) {
		if e, ok := e.(pointer.Event); ok {
			if e.Scroll.Y < 0 {
				ns := b.scale + 0.1
				if ns > 1.0 {
					ns = 1.0
				}
				if b.scale != ns {
					b.xOff = (e.Position.X-b.xOff)/b.scale*ns*-1 + e.Position.X
					b.yOff = (e.Position.Y-b.yOff)/b.scale*ns*-1 + e.Position.Y
					b.scale = ns
				}
			} else if e.Scroll.Y > 0 {
				ns := b.scale - 0.2
				if ns < scaleMin {
					ns = scaleMin
				}
				if !(ns > scaleMin) {
					b.applyMinScale(gtx, scaleMin)
				} else {
					//TODO Zooming out, we should probably try to center the picture?
					b.xOff = (e.Position.X-b.xOff)/b.scale*ns*-1 + e.Position.X
					b.yOff = (e.Position.Y-b.yOff)/b.scale*ns*-1 + e.Position.Y
					b.scale = ns
				}
			}
		}
	}

	// Confine the area for pointer events, this confinement is kept though transform, so pop
	pointer.Rect(image.Rect(0, 0, gtx.Constraints.Max.X, gtx.Constraints.Max.Y)).Add(gtx.Ops)
	pointer.InputOp{Tag: b, Types: pointer.Scroll}.Add(gtx.Ops)

	clip.RRect{Rect: f32.Rectangle{Min: f32.Point{X: 0, Y: 0},
		Max: f32.Point{X: float32(gtx.Constraints.Max.X),
			Y: float32(gtx.Constraints.Max.Y)}}}.Add(gtx.Ops)

	op.Affine(f32.Affine2D{}.Scale(f32.Point{}, f32.Point{b.scale, b.scale}).Offset(f32.Point{b.xOff, b.yOff})).Add(gtx.Ops)

	pointer.Rect(image.Rect(0, 0, b.Board.W*b.tileSize, b.Board.H*b.tileSize)).Add(gtx.Ops)
	pointer.InputOp{Tag: b.Board, Types: pointer.Press | pointer.Leave | pointer.Move}.Add(gtx.Ops)

	//here we loop through all the events associated.
	//Events should be in post-transform co-ordinates, except scroll
	for _, e := range gtx.Events(b.Board) {
		if e, ok := e.(pointer.Event); ok {
			if e.Type == pointer.Press && e.Buttons.Contain(pointer.ButtonLeft) {
				x := int(e.Position.X) / b.tileSize
				y := int(e.Position.Y) / b.tileSize
				fmt.Printf("%v, %v\n", x, y)
				r := image.Rect(x-b.selImg.W+1, y-b.selImg.H+1, x+1, y+1)
				if (b.selImg == Tile{}) {
					r = image.Rect(x, y, x+1, y+1)
				}
				for key, blocks := range b.Board.Layers[b.currentLayer] {
					temp := blocks[:0]
					for _, block := range blocks {
						if !(r.Overlaps(image.Rect(block.X, block.Y, block.X+block.W, block.Y+block.H))) {
							temp = append(temp, block)
						}
					}
					if len(temp) < len(blocks) {
						b.Board.Layers[b.currentLayer][key] = temp
					}
				}
				if (b.selImg != Tile{}) {
					b.Board.AddTile(b.selImg, b.currentLayer, x-b.selImg.W+1, y-b.selImg.H+1)
				}
				b.blockLight = b.blockLight[:0]
			} else if e.Type == pointer.Move {
				x := int(e.Position.X) / b.tileSize
				y := int(e.Position.Y) / b.tileSize

				if x != b.highLight.X || y != b.highLight.Y {
					b.highLight.X = x
					b.highLight.Y = y
					r := image.Rect(x-b.selImg.W+1, y-b.selImg.H+1, x+1, y+1)
					if (b.selImg == Tile{}) {
						r = image.Rect(x, y, x+1, y+1)
					}
					for _, blocks := range b.Board.Layers[b.currentLayer] {
						for _, block := range blocks {
							r2 := image.Rect(block.X, block.Y, block.X+block.W, block.Y+block.H)
							if r.Overlaps(r2) {
								block.conflict = true
							} else {
								block.conflict = false
							}
						}
					}
				}
			} else if e.Type == pointer.Leave {
				b.highLight.X = -1
				b.highLight.Y = -1
			}
		}
	}

	clip.RRect{Rect: f32.Rectangle{Min: f32.Point{X: 0, Y: 0},
		Max: f32.Point{X: float32(b.Board.W * b.tileSize),
			Y: float32(b.Board.H * b.tileSize)}}}.Add(gtx.Ops)

	b.drawBoard(gtx)

	return layout.Dimensions{Size: image.Point{X: gtx.Constraints.Max.X, Y: gtx.Constraints.Max.Y}}
}

func (b *BoardEditor) applyMinScale(gtx layout.Context, scaleMin float32) {
	b.scale = scaleMin

	w := int(float32(b.Board.W*b.tileSize) * b.scale)
	h := int(float32(b.Board.H*b.tileSize) * b.scale)

	if w <= gtx.Constraints.Max.X {
		no := (gtx.Constraints.Max.X - w) / 2
		b.xOff = float32(no)
	}
	if h <= gtx.Constraints.Max.Y {
		no := (gtx.Constraints.Max.Y - h) / 2
		b.yOff = float32(no)
	}
}

func (b *BoardEditor) drawBoard(gtx layout.Context) {

	paint.ColorOp{Color: color.RGBA{A: 0xFF}}.Add(gtx.Ops)
	paint.PaintOp{Rect: f32.Rect(0, 0, float32(b.tileSize*b.Board.W), float32(b.tileSize*b.Board.H))}.Add(gtx.Ops)

	for _, layer := range b.Board.Layers {
		for key, blocks := range layer {
			img := asset.GetImageConflict(key)
			paint.NewImageOp(img).Add(gtx.Ops)
			for _, block := range blocks {
				if block.conflict {
					paint.PaintOp{Rect: f32.Rect(float32(block.X*b.tileSize), float32(block.Y*b.tileSize), float32((block.X+block.W)*b.tileSize), float32((block.Y+block.H)*b.tileSize))}.Add(gtx.Ops)
				}
			}

			img = asset.GetImage(key)
			paint.NewImageOp(img).Add(gtx.Ops)
			for _, block := range blocks {
				if !block.conflict {
					paint.PaintOp{Rect: f32.Rect(float32(block.X*b.tileSize), float32(block.Y*b.tileSize), float32((block.X+block.W)*b.tileSize), float32((block.Y+block.H)*b.tileSize))}.Add(gtx.Ops)
				}
			}
		}
	}

	if b.selImg != (Tile{}) {
		b.selImgRdr = asset.GetImageOverlay(b.selImg.ImgFile)
		paint.NewImageOp(b.selImgRdr).Add(gtx.Ops)
		paint.PaintOp{Rect: f32.Rect(float32((b.highLight.X-b.selImg.W+1)*b.tileSize), float32((b.highLight.Y-b.selImg.H+1)*b.tileSize), float32((b.highLight.X+1)*b.tileSize), float32((b.highLight.Y+1)*b.tileSize))}.Add(gtx.Ops)
	} else {
		b.selImgRdr = nil
	}

	//Highlight the mouse-over
	if b.highLight.X > -1 {
		paint.ColorOp{Color: color.RGBA{B: 0xFF, A: 0x33}}.Add(gtx.Ops)
		paint.PaintOp{Rect: f32.Rect(float32(b.tileSize*b.highLight.X), float32(b.highLight.Y*b.tileSize), float32(b.tileSize*(b.highLight.X+1)), float32(b.tileSize*(b.highLight.Y+1)))}.Add(gtx.Ops)
	}

	//Paint the Grid
	paint.ColorOp{Color: Grey}.Add(gtx.Ops)
	for i := 1; i < b.Board.W; i++ {
		paint.PaintOp{Rect: f32.Rect(float32(b.tileSize*i), 0, float32(b.tileSize*i+1), float32(b.tileSize*(b.Board.H)))}.Add(gtx.Ops)
	}

	for i := 1; i < b.Board.H; i++ {
		paint.PaintOp{Rect: f32.Rect(0, float32(b.tileSize*i), float32(b.tileSize*(b.Board.W)), float32(b.tileSize*i+1))}.Add(gtx.Ops)
	}

}
