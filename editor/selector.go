package editor

import (
	"gotimap/asset"
	"image"

	"image/color"

	"gioui.org/f32"
	"gioui.org/io/pointer"
	"gioui.org/layout"
	"gioui.org/op"
	"gioui.org/op/paint"
)

type Tile struct {
	ImgFile string
	W, H    int
}

func NewTileList() *TileList {
	t := &TileList{tiles: make([]*tileElement, 0), list: layout.List{Axis: layout.Vertical}, SelectedTile: make(chan Tile, 4)}
	t.tiles = append(t.tiles, &tileElement{tile: Tile{}, selected: false, list: t})
	return t
}

func (t *TileList) AddTile(imgFile string, w int, h int) {
	t.tiles = append(t.tiles, &tileElement{tile: Tile{ImgFile: imgFile, H: h, W: w}, selected: false, list: t})
}

type tileElement struct {
	tile     Tile
	selected bool
	list     *TileList
}

type TileList struct {
	tiles []*tileElement
	list  layout.List

	SelectedTile chan (Tile)
}

func (t *TileList) Widget() layout.Widget {
	return func(gtx layout.Context) layout.Dimensions { return t.Layout(gtx) }
}

func (t *TileList) Layout(gtx layout.Context) layout.Dimensions {
	for _, e := range gtx.Events(t) {
		if e, ok := e.(pointer.Event); ok {
			if e.Type == pointer.Press && e.Buttons.Contain(pointer.ButtonLeft) {
				for _, te := range t.tiles {
					te.selected = false
				}
			}
		}
	}

	s := op.Push(gtx.Ops)
	pointer.Rect(image.Rect(0, 0, 300, gtx.Constraints.Max.Y)).Add(gtx.Ops)
	pointer.PassOp{Pass: true}.Add(gtx.Ops)
	pointer.InputOp{Tag: t, Types: pointer.Press}.Add(gtx.Ops)
	s.Pop()

	return t.list.Layout(gtx, len(t.tiles), func(gtx layout.Context, i int) layout.Dimensions {
		return t.tiles[i].Layout(gtx)
	})

}

func (t *tileElement) Layout(gtx layout.Context) layout.Dimensions {
	//here we loop through all the events associated.
	for _, e := range gtx.Events(t) {
		if e, ok := e.(pointer.Event); ok {
			if e.Type == pointer.Press && e.Buttons.Contain(pointer.ButtonLeft) {
				if !t.selected {
					for _, te := range t.list.tiles {
						if te.selected {
							te.selected = false
						}
					}
					t.selected = true
					t.list.SelectedTile <- t.tile
				}
			}
		}
	}

	s := op.Push(gtx.Ops)
	pointer.Rect(image.Rect(0, 0, 300, 200)).Add(gtx.Ops)
	pointer.InputOp{Tag: t, Types: pointer.Press | pointer.Leave | pointer.Enter}.Add(gtx.Ops)
	s.Pop()

	img := asset.GetThumbnail(t.tile.ImgFile)
	paint.NewImageOp(img).Add(gtx.Ops)

	i := float32(0)
	j := float32(0)
	x := float32(200)
	y := float32(200)

	if t.tile.H > t.tile.W {
		x = float32(y) * float32(t.tile.W) / float32(t.tile.H)
		i = (200 - x) / 2
		x = (200 + x) / 2
	} else if t.tile.W > t.tile.H {
		y = float32(x) * float32(t.tile.H) / float32(t.tile.W)
		i = (200 - y) / 2
		y = (200 + y) / 2
	}
	paint.PaintOp{Rect: f32.Rect(i, j, x, y)}.Add(gtx.Ops)

	if t.selected {
		paint.ColorOp{Color: color.RGBA{B: 0xFF, A: 0x33}}.Add(gtx.Ops)
		paint.PaintOp{Rect: f32.Rect(0, 0, 300, 300)}.Add(gtx.Ops)
	}

	return layout.Dimensions{Size: image.Point{X: 300, Y: 200}}
}
