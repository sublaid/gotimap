package asset

import (
	"fmt"
	"image"
	"image/color"
	"image/draw"
	_ "image/jpeg"
	_ "image/png"
	"os"
)

var images map[string]image.Image = make(map[string]image.Image)
var imagesOverlay map[string]image.Image = make(map[string]image.Image)
var imagesConflict map[string]image.Image = make(map[string]image.Image)

func GetImageOverlay(key string) image.Image {
	if imagesOverlay[key] != nil {
		return imagesOverlay[key]
	}

	existingImageFile, err := os.Open(key)
	if err != nil {
		fmt.Println("Unable to open file")
		imagesOverlay[key] = image.Transparent
		return image.Transparent
	}
	defer existingImageFile.Close()

	src, _, err := image.Decode(existingImageFile)
	if err != nil {
		fmt.Println("Unable to decode file", err)
		imagesOverlay[key] = image.Transparent
		return image.Transparent
	}

	switch src := src.(type) {
	case *image.Uniform:
		imagesOverlay[key] = src
	case *image.RGBA:
		mask := image.NewUniform(color.Alpha{128})
		m := image.NewRGBA(src.Bounds())
		draw.DrawMask(m, m.Bounds(), src, image.Point{}, mask, image.Point{}, draw.Over)
		imagesOverlay[key] = m
	default:
		mask := image.NewUniform(color.Alpha{128})
		m := image.NewRGBA(src.Bounds())
		draw.DrawMask(m, m.Bounds(), src, image.Point{}, mask, image.Point{}, draw.Over)
		imagesOverlay[key] = m
	}

	return imagesOverlay[key]
}

func GetImage(key string) image.Image {
	if images[key] != nil {
		return images[key]
	}

	existingImageFile, err := os.Open(key)
	if err != nil {
		fmt.Println("Unable to open file")
		images[key] = image.Transparent
		return image.Transparent
	}
	defer existingImageFile.Close()

	src, _, err := image.Decode(existingImageFile)
	if err != nil {
		fmt.Println("Unable to decode file", err)
		images[key] = image.Transparent
		return image.Transparent
	}

	switch src := src.(type) {
	case *image.Uniform:
		images[key] = src
	case *image.RGBA:
		images[key] = src
	default:
		sz := src.Bounds().Size()
		dst := image.NewRGBA(image.Rectangle{
			Max: sz,
		})
		draw.Draw(dst, dst.Bounds(), src, src.Bounds().Min, draw.Src)
		images[key] = dst
	}

	return images[key]
}

func GetImageConflict(key string) image.Image {
	if imagesConflict[key] != nil {
		return imagesConflict[key]
	}

	existingImageFile, err := os.Open(key)
	if err != nil {
		fmt.Println("Unable to open file")
		imagesConflict[key] = image.Transparent
		return image.Transparent
	}
	defer existingImageFile.Close()

	src, _, err := image.Decode(existingImageFile)
	if err != nil {
		fmt.Println("Unable to decode file", err)
		imagesConflict[key] = image.Transparent
		return image.Transparent
	}

	switch src := src.(type) {
	case *image.Uniform:
		imagesConflict[key] = image.NewUniform(color.RGBA{R: 0xFF, A: 0x33})
	case *image.RGBA:
		mask := image.NewUniform(color.RGBA{R: 0xFF, A: 0x33})
		draw.Draw(src, src.Bounds(), mask, src.Bounds().Min, draw.Over)
		imagesOverlay[key] = src
	default:
		dst := image.NewRGBA(image.Rectangle{Max: src.Bounds().Max})
		draw.Draw(dst, dst.Bounds(), src, src.Bounds().Min, draw.Src)
		draw.DrawMask(dst, dst.Bounds(), image.NewUniform(color.RGBA{R: 0xFF, A: 0x33}), src.Bounds().Min, image.NewUniform(color.Alpha{128}), src.Bounds().Min, draw.Over)
		imagesConflict[key] = dst
	}

	return imagesConflict[key]
}
