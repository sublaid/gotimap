package asset

import (
	"fmt"
	"image"
	"image/draw"
	_ "image/jpeg"
	_ "image/png"
	"math"
	"os"
	"runtime"
	"sync"
)

var imagesThumbnail map[string]image.Image = make(map[string]image.Image)

//Rendered over a 200x200 square
func GetThumbnail(key string) image.Image {
	if imagesThumbnail[key] != nil {
		return imagesThumbnail[key]
	}

	existingImageFile, err := os.Open(key)
	if err != nil {
		fmt.Println("Unable to open file")
		imagesThumbnail[key] = image.Transparent
		return image.Transparent
	}
	defer existingImageFile.Close()

	src, _, err := image.Decode(existingImageFile)
	if err != nil {
		fmt.Println("Unable to decode file", err)
		imagesThumbnail[key] = image.Transparent
		return image.Transparent
	}

	switch src := src.(type) {
	case *image.Uniform:
		imagesThumbnail[key] = src
	case *image.RGBA:
		if src.Bounds().Dx() > ThumbNailSize || src.Bounds().Dy() > ThumbNailSize {
			if src.Bounds().Dx() > src.Bounds().Dy() {
				src = resizeNearest(ThumbNailSize, 0, src)
			} else {
				src = resizeNearest(0, ThumbNailSize, src)
			}
		}
		imagesThumbnail[key] = src
	default:
		sz := src.Bounds().Size()
		dst := image.NewRGBA(image.Rectangle{
			Max: sz,
		})
		draw.Draw(dst, dst.Bounds(), src, src.Bounds().Min, draw.Src)
		if dst.Bounds().Dx() > ThumbNailSize || dst.Bounds().Dy() > ThumbNailSize {
			if dst.Bounds().Dx() > dst.Bounds().Dy() {
				dst = resizeNearest(ThumbNailSize, 0, dst)
			} else {
				dst = resizeNearest(0, ThumbNailSize, dst)
			}
		}
		imagesThumbnail[key] = dst
	}

	return imagesThumbnail[key]
}

var ThumbNailSize int = 200

func floatToUint8(x float32) uint8 {
	// Nearest-neighbor values are always
	// positive no need to check lower-bound.
	if x > 0xfe {
		return 0xff
	}
	return uint8(x)
}

func nearestRGBA(in *image.RGBA, out *image.RGBA, scale float64, coeffs []bool, offset []int, filterLength int) {
	newBounds := out.Bounds()
	maxX := in.Bounds().Dx() - 1

	for x := newBounds.Min.X; x < newBounds.Max.X; x++ {
		row := in.Pix[x*in.Stride:]
		for y := newBounds.Min.Y; y < newBounds.Max.Y; y++ {
			var rgba [4]float32
			var sum float32
			start := offset[y]
			ci := y * filterLength
			for i := 0; i < filterLength; i++ {
				if coeffs[ci+i] {
					xi := start + i
					switch {
					case uint(xi) < uint(maxX):
						xi *= 4
					case xi >= maxX:
						xi = 4 * maxX
					default:
						xi = 0
					}
					rgba[0] += float32(row[xi+0])
					rgba[1] += float32(row[xi+1])
					rgba[2] += float32(row[xi+2])
					rgba[3] += float32(row[xi+3])
					sum++
				}
			}

			xo := (y-newBounds.Min.Y)*out.Stride + (x-newBounds.Min.X)*4
			out.Pix[xo+0] = floatToUint8(rgba[0] / sum)
			out.Pix[xo+1] = floatToUint8(rgba[1] / sum)
			out.Pix[xo+2] = floatToUint8(rgba[2] / sum)
			out.Pix[xo+3] = floatToUint8(rgba[3] / sum)
		}
	}
}

// Calculates scaling factors using old and new image dimensions.
func calcFactors(width, height int, oldWidth, oldHeight float64) (scaleX, scaleY float64) {
	if width == 0 {
		if height == 0 {
			scaleX = 1.0
			scaleY = 1.0
		} else {
			scaleY = oldHeight / float64(height)
			scaleX = scaleY
		}
	} else {
		scaleX = oldWidth / float64(width)
		if height == 0 {
			scaleY = scaleX
		} else {
			scaleY = oldHeight / float64(height)
		}
	}
	return
}

var blur = 1.0

func createWeightsNearest(dy, filterLength int, blur, scale float64) ([]bool, []int, int) {
	filterLength = filterLength * int(math.Max(math.Ceil(blur*scale), 1))
	filterFactor := math.Min(1./(blur*scale), 1)

	coeffs := make([]bool, dy*filterLength)
	start := make([]int, dy)
	for y := 0; y < dy; y++ {
		interpX := scale*(float64(y)+0.5) - 0.5
		start[y] = int(interpX) - filterLength/2 + 1
		interpX -= float64(start[y])
		for i := 0; i < filterLength; i++ {
			in := (interpX - float64(i)) * filterFactor
			if in >= -0.5 && in < 0.5 {
				coeffs[y*filterLength+i] = true
			} else {
				coeffs[y*filterLength+i] = false
			}
		}
	}

	return coeffs, start, filterLength
}

func resizeNearest(width, height int, img *image.RGBA) *image.RGBA {
	scaleX, scaleY := calcFactors(width, height, float64(img.Bounds().Dx()), float64(img.Bounds().Dy()))
	if width == 0 {
		width = int(0.7 + float64(img.Bounds().Dx())/scaleX)
	}
	if height == 0 {
		height = int(0.7 + float64(img.Bounds().Dy())/scaleY)
	}

	taps := 2
	cpus := runtime.GOMAXPROCS(0)
	wg := sync.WaitGroup{}

	// 8-bit precision
	temp := image.NewRGBA(image.Rect(0, 0, img.Bounds().Dy(), int(width)))
	result := image.NewRGBA(image.Rect(0, 0, int(width), int(height)))

	// horizontal filter, results in transposed temporary image
	coeffs, offset, filterLength := createWeightsNearest(temp.Bounds().Dy(), taps, blur, scaleX)
	wg.Add(cpus)
	for i := 0; i < cpus; i++ {
		slice := makeSlice(temp, i, cpus).(*image.RGBA)
		go func() {
			defer wg.Done()
			nearestRGBA(img, slice, scaleX, coeffs, offset, filterLength)
		}()
	}
	wg.Wait()

	// horizontal filter on transposed image, result is not transposed
	coeffs, offset, filterLength = createWeightsNearest(result.Bounds().Dy(), taps, blur, scaleY)
	wg.Add(cpus)
	for i := 0; i < cpus; i++ {
		slice := makeSlice(result, i, cpus).(*image.RGBA)
		go func() {
			defer wg.Done()
			nearestRGBA(temp, slice, scaleY, coeffs, offset, filterLength)
		}()
	}
	wg.Wait()
	return result
}

type imageWithSubImage interface {
	image.Image
	SubImage(image.Rectangle) image.Image
}

func makeSlice(img imageWithSubImage, i, n int) image.Image {
	return img.SubImage(image.Rect(img.Bounds().Min.X, img.Bounds().Min.Y+i*img.Bounds().Dy()/n, img.Bounds().Max.X, img.Bounds().Min.Y+(i+1)*img.Bounds().Dy()/n))
}
