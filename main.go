package main

import (
	"image"
	"log"
	"os"

	"gioui.org/app"
	"gioui.org/f32"
	"gioui.org/io/system"
	"gioui.org/layout"
	"gioui.org/op"
	"gioui.org/op/paint"
	"gioui.org/unit"

	"gotimap/editor"
)

var board *editor.BoardEditor
var selector *editor.TileList

func main() {
	selector = editor.NewTileList()
	selector.AddTile("img/112107-DaSIG Tunnel Tiled End 2 2x4.jpg", 2, 4)
	selector.AddTile("img/112110-DaSIG Tunnel Tiled Expanded 1 3x6.jpg", 3, 6)
	selector.AddTile("img/112109-DaSIG Tunnel Tiled End 4 2x4.jpg", 2, 4)
	selector.AddTile("img/111900-DaSIG  Water Unfunrsihed 30x30.jpg", 30, 30)
	selector.AddTile("img/111901-DaSIG Airlock 15x15.jpg", 15, 15)

	board = editor.NewBoardEditor(50, 50, selector)

	go func() {
		w := app.NewWindow(app.Size(unit.Dp(800), unit.Dp(700)))
		if err := loop(w); err != nil {
			log.Fatal(err)
		}
		os.Exit(0)
	}()
	app.Main()
}

func loop(w *app.Window) error {
	var ops op.Ops

	for {
		select {
		case e := <-w.Events():
			switch e := e.(type) {
			case system.DestroyEvent:
				return e.Err
			case system.FrameEvent:
				gtx := layout.NewContext(&ops, e)
				layout.Flex{}.Layout(gtx,
					layout.Flexed(1, board.Widget()),
					layout.Rigid(func(gtx layout.Context) layout.Dimensions {
						paint.ColorOp{Color: editor.Grey}.Add(gtx.Ops)
						paint.PaintOp{Rect: f32.Rect(0, 0, 1, float32(gtx.Constraints.Max.Y))}.Add(gtx.Ops)
						return layout.Dimensions{Size: image.Point{X: 1, Y: gtx.Constraints.Max.Y}}
					}),
					layout.Rigid(selector.Widget()))
				e.Frame(gtx.Ops)
			}
		}
	}
}
